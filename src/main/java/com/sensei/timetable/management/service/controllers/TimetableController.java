package com.sensei.timetable.management.service.controllers;

import com.sensei.timetable.management.service.models.NonRecurringSession;
import com.sensei.timetable.management.service.services.TimetableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/v1/timetable")
public class TimetableController {

	@Autowired
	TimetableService timetableService;

	@GetMapping(produces = APPLICATION_JSON_VALUE)
	public List<NonRecurringSession> getTimetable(@RequestParam @DateTimeFormat(pattern = "YYYY-MM-DD") LocalDate from,
												  @RequestParam @DateTimeFormat(pattern = "YYYY-MM-DD") LocalDate to) {
		return timetableService.getTimetable(from, to);
	}
}
