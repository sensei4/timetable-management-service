package com.sensei.timetable.management.service.models.exceptions;

public class IllegalSessionUpdateException extends RuntimeException {
	public IllegalSessionUpdateException(String message) {
		super(message);
	}
}
