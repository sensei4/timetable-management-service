package com.sensei.timetable.management.service.controllers;

import com.sensei.timetable.management.service.models.Session;
import com.sensei.timetable.management.service.models.exceptions.IllegalSessionUpdateException;
import com.sensei.timetable.management.service.models.exceptions.SessionNotFoundException;
import com.sensei.timetable.management.service.services.SessionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;


@RestController
@RequestMapping("/api/v1/sessions")
public class SessionController {

	@Autowired
	private SessionService sessionService;

	@PostMapping(consumes = {APPLICATION_JSON_VALUE},
		produces = {APPLICATION_JSON_VALUE})
	@ResponseStatus(HttpStatus.CREATED)
	public Session saveNewSession(@RequestBody Session session) {
		if (session.getId() != null)
			throw new IllegalArgumentException("ID cannot be set on a new session");

		return sessionService.saveSession(session);
	}

	@GetMapping(produces = {APPLICATION_JSON_VALUE})
	public List<Session> getAllSessions() {
		return sessionService.getAllSessions();
	}

	@GetMapping(value = "/{id}",
		produces = {APPLICATION_JSON_VALUE})
	public Session getSession(@PathVariable String id) {
		try {
			return sessionService.getSession(id);
		} catch (SessionNotFoundException e) {
			throw new ResponseStatusException(
				HttpStatus.NOT_FOUND, "Session Not Found", e);
		}
	}

	@PutMapping(value = "/{id}",
		consumes = {APPLICATION_JSON_VALUE},
		produces = {APPLICATION_JSON_VALUE})
	public Session updateSession(@PathVariable String id, @RequestBody Session updatedSession) throws SessionNotFoundException {
		if (!id.equals(updatedSession.getId()))
			throw new IllegalSessionUpdateException("ID must match in path and body");

		return sessionService.updateSession(id, updatedSession);
	}

	@DeleteMapping(value = "/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteSession(@PathVariable String id) {
		sessionService.deleteSession(id);
	}

}
