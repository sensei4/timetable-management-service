package com.sensei.timetable.management.service.services;

import com.sensei.timetable.management.service.models.Session;
import com.sensei.timetable.management.service.models.exceptions.IllegalSessionUpdateException;
import com.sensei.timetable.management.service.models.exceptions.SessionNotFoundException;
import com.sensei.timetable.management.service.repositories.SessionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SessionService {

	private static Logger LOGGER = LoggerFactory.getLogger(SessionService.class);

	@Autowired
	private SessionRepository repository;

	public Session saveSession(Session session) {
		return repository.save(session);
	}

	public List<Session> getAllSessions() {
		return repository.findAll();
	}

	public Session getSession(String id) throws SessionNotFoundException {
		return repository.findById(id).orElseThrow(() -> new SessionNotFoundException("Session not found for id: %s".formatted(id)));
	}

	public Session updateSession(String id, Session updatedSession) throws SessionNotFoundException {
		final Session existingSession = getSession(id);

		if (existingSession.getClass() != updatedSession.getClass())
			throw new IllegalSessionUpdateException("Session type is immutable");

		LOGGER.info("Updating session: {}, {}", id, existingSession.getDiffMessage(updatedSession));

		return saveSession(updatedSession);
	}

	public void deleteSession(String id) {
		LOGGER.info("Deleting session: {}", id);

		repository.deleteById(id);
	}
}
