package com.sensei.timetable.management.service.models;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.builder.DiffResult;
import org.apache.commons.lang3.builder.ReflectionDiffBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
	@JsonSubTypes.Type(value = RecurringSession.class, name = "recurring"),
	@JsonSubTypes.Type(value = NonRecurringSession.class, name = "non-recurring")
})
public abstract class Session {

	@Id
	private String id;
	private String title;
	private String coach;
	private Duration duration;

	public List<String> getDiffMessage(Session updatedSession) {
		final DiffResult<Session> differences = this.diff(updatedSession);
		return differences.getDiffs().stream()
			.map(diff -> "Updating %s from '%s' to '%s'".formatted(diff.getFieldName(), diff.getLeft(), diff.getRight()))
			.toList();
	}

	private DiffResult<Session> diff(Session obj) {
		return new ReflectionDiffBuilder<>(this, obj, ToStringStyle.JSON_STYLE)
			.build();
	}
}
