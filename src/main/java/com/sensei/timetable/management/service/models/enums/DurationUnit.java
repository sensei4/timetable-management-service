package com.sensei.timetable.management.service.models.enums;

public enum DurationUnit {
	MINUTES,
	HOURS
}
