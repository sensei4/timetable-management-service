package com.sensei.timetable.management.service.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.security.Key;

@Component
public class JwtAuthFilter extends OncePerRequestFilter {

	public static final String AUTHORIZATION = "Authorization";
	public static final String TOKEN_PREFIX = "Bearer ";

	@Value("${application.security.jwt.shared-secret}")
	private String sharedSecret;

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
		throws ServletException, IOException {

		final String authHeader = req.getHeader(AUTHORIZATION);
		if (authHeader == null){
			throw new ServletException("Missing Authorization header.");
		}
		if ( !authHeader.startsWith(TOKEN_PREFIX)) {
			throw new ServletException("Invalid Authorization header.");
		}

		final String token = authHeader.substring(7); // The part after "Bearer "
		try {
			final Claims claims = Jwts.parserBuilder()
				.setSigningKey(getSigningKey())
				.build()
				.parseClaimsJws(token).getBody();
			req.setAttribute("claims", claims);
		} catch (final SignatureException e) {
			throw new ServletException("Invalid token.");
		}

		chain.doFilter(req, res);
	}

	private Key getSigningKey() {
		byte[] keyBytes = Decoders.BASE64.decode(sharedSecret);
		return Keys.hmacShaKeyFor(keyBytes);
	}
}
