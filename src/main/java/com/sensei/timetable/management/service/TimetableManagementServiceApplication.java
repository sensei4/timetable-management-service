package com.sensei.timetable.management.service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.UserDetailsServiceAutoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories("com.sensei.timetable.management.service.repositories")
@EnableAutoConfiguration(exclude = UserDetailsServiceAutoConfiguration.class)
public class TimetableManagementServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TimetableManagementServiceApplication.class, args);
	}

}
