package com.sensei.timetable.management.service.models;

import com.sensei.timetable.management.service.models.enums.DurationUnit;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Duration {

	private int value;
	private DurationUnit unit;

}
