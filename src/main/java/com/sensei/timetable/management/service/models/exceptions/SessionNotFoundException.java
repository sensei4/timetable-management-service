package com.sensei.timetable.management.service.models.exceptions;

public class SessionNotFoundException extends RuntimeException {

	public SessionNotFoundException(String message) {
		super(message);
	}
}
