package com.sensei.timetable.management.service.models.modifiers;


import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDate;
import java.util.List;

@Data
@NoArgsConstructor
@SuperBuilder
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
	@JsonSubTypes.Type(value = Cancelled.class, name = "cancellation"),
	@JsonSubTypes.Type(value = CoachChange.class, name = "coachChange"),
	@JsonSubTypes.Type(value = StartTimeChange.class, name = "timeChange")
})
public class ScheduleModifier {
	private List<LocalDate> affectedDates;

}
