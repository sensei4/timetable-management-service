package com.sensei.timetable.management.service.models.enums;

public enum ScheduleFrequency {
	DAILY,
	WEEKLY,
	MONTHLY
}
