package com.sensei.timetable.management.service.repositories;

import com.sensei.timetable.management.service.models.Session;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SessionRepository extends MongoRepository<Session, String> {
}
