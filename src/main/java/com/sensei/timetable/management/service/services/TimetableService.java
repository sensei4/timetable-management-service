package com.sensei.timetable.management.service.services;

import com.sensei.timetable.management.service.models.NonRecurringSession;
import com.sensei.timetable.management.service.models.RecurringSession;
import com.sensei.timetable.management.service.models.Schedule;
import com.sensei.timetable.management.service.models.Session;
import com.sensei.timetable.management.service.models.modifiers.Cancelled;
import com.sensei.timetable.management.service.models.modifiers.CoachChange;
import com.sensei.timetable.management.service.models.modifiers.ScheduleModifier;
import com.sensei.timetable.management.service.models.modifiers.StartTimeChange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Objects.isNull;

@Service
public class TimetableService {

	@Autowired
	private SessionService sessionService;

	public List<NonRecurringSession> getTimetable(LocalDate from, LocalDate until) {
		List<Session> sessions = sessionService.getAllSessions();
		List<NonRecurringSession> timetable = new ArrayList<>();

		sessions.stream()
			.filter(NonRecurringSession.class::isInstance)
			.map(NonRecurringSession.class::cast)
			.filter(session -> isBetweenInclusive(session.getDate(), from, until))
			.forEach(timetable::add);

		sessions.stream()
			.filter(RecurringSession.class::isInstance)
			.map(RecurringSession.class::cast)
			.filter(recurringSession -> recurringScheduleHasStarted(recurringSession, until))
			.map(recurring -> mapToSingularSessions(recurring, from, until))
			.flatMap(List::stream)
			.forEach(timetable::add);

		return timetable;
	}

	private boolean recurringScheduleHasStarted(RecurringSession recurringSession, LocalDate date) {
		LocalDate scheduleStart = recurringSession.getSchedule().getFrom();

		return scheduleStart.isEqual(date) || scheduleStart.isBefore(date);
	}

	private boolean isBetweenInclusive(LocalDate value, LocalDate lowerLimit, LocalDate upperLimit) {
		return (value.isEqual(lowerLimit) || value.isAfter(lowerLimit))
			&& (value.isEqual(upperLimit) || value.isBefore(upperLimit));
	}

	private List<NonRecurringSession> mapToSingularSessions(RecurringSession recurringSession, LocalDate from, LocalDate until) {
		Schedule schedule = recurringSession.getSchedule();

		return Stream.iterate(from, date -> date.plusDays(1))
			.limit(ChronoUnit.DAYS.between(from, until) + 1)
			.filter(day -> isBetweenInclusive(day, schedule.getFrom(), schedule.getUntil())
				&& isNotCancelledOnDate(schedule.getModifications(), day)
				&& switch (schedule.getFrequency()) {
				case DAILY -> true; // No other filter required
				case WEEKLY -> day.getDayOfWeek().equals(schedule.getFrom().getDayOfWeek());
				case MONTHLY -> day.getDayOfMonth() == schedule.getFrom().getDayOfMonth();
			})
			.map(day -> createNonRecurringSessionOnDay(day, recurringSession))
			.collect(Collectors.toList());
	}

	private NonRecurringSession createNonRecurringSessionOnDay(LocalDate day, RecurringSession recurringSession) {
		List<ScheduleModifier> modifiers = Optional.ofNullable(recurringSession.getSchedule().getModifications())
			.orElse(Collections.emptyList());

		List<ScheduleModifier> applicableModifiers = modifiers.stream()
			.filter(modifier -> modifier.getAffectedDates().contains(day))
			.toList();

		String coach = applicableModifiers.stream()
			.filter(CoachChange.class::isInstance)
			.findFirst()
			.map(CoachChange.class::cast)
			.map(CoachChange::getNewCoach)
			.orElse(recurringSession.getCoach());

		LocalTime startTime = applicableModifiers.stream()
			.filter(StartTimeChange.class::isInstance)
			.findFirst()
			.map(StartTimeChange.class::cast)
			.map(StartTimeChange::getNewStartTime)
			.orElse(recurringSession.getSchedule().getStartTime());

		return NonRecurringSession.builder()
			.id("%s:%s".formatted(recurringSession.getId(),
				day.format(DateTimeFormatter.ISO_LOCAL_DATE)))
			.title(recurringSession.getTitle())
			.coach(coach)
			.date(day)
			.startTime(startTime)
			.duration(recurringSession.getDuration())
			.build();
	}

	private boolean isNotCancelledOnDate(List<ScheduleModifier> modifiers, LocalDate date) {
		if (isNull(modifiers) || modifiers.isEmpty())
			return true;

		return modifiers.stream()
			.filter(Cancelled.class::isInstance)
			.noneMatch(modifier -> modifier.getAffectedDates().contains(date));
	}
}
