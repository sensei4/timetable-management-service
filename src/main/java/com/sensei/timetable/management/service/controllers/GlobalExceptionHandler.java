package com.sensei.timetable.management.service.controllers;

import com.sensei.timetable.management.service.models.exceptions.IllegalSessionUpdateException;
import com.sensei.timetable.management.service.models.exceptions.SessionNotFoundException;
import lombok.Data;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler({IllegalArgumentException.class, IllegalSessionUpdateException.class})
	protected ResponseEntity<Error> handleIllegalArgument(RuntimeException exception) {
		return ResponseEntity.badRequest()
			.contentType(MediaType.APPLICATION_JSON)
			.body(new Error(exception.getMessage()));
	}

	@ExceptionHandler(SessionNotFoundException.class)
	protected ResponseEntity<?> handleSessionNotFound() {
		return new ResponseEntity<>(NOT_FOUND);
	}

	@Data
	static class Error {
		final String error;
	}
}