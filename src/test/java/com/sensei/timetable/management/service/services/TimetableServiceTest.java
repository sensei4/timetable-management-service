package com.sensei.timetable.management.service.services;

import com.sensei.timetable.management.service.models.NonRecurringSession;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

import static com.sensei.timetable.management.service.TestConstants.CANCELLATION_DATE;
import static com.sensei.timetable.management.service.TestConstants.CHANGE_DATE;
import static com.sensei.timetable.management.service.TestConstants.NON_RECURRING_SESSION_WITH_ID;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_MONTHLY;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_WEEKLY;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_WITH_ID;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_WITH_MODS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TimetableServiceTest {

	@Mock
	SessionService sessionService;

	@InjectMocks
	TimetableService timetableService;

	@Test
	public void canGetTimetableWithNonRecurringSession() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(NON_RECURRING_SESSION_WITH_ID));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2030, 1, 1),
			LocalDate.of(2030, 1, 7));

		assert(List.of(NON_RECURRING_SESSION_WITH_ID).equals(result));
	}

	@Test
	public void canGetTimetableWithDailyRecurringSessions() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_WITH_ID));
		List<LocalDate> expectedDates = List.of(
			LocalDate.of(2030, 1, 1),
			LocalDate.of(2030, 1, 2),
			LocalDate.of(2030, 1, 3),
			LocalDate.of(2030, 1, 4),
			LocalDate.of(2030, 1, 5),
			LocalDate.of(2030, 1, 6),
			LocalDate.of(2030, 1, 7));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2030, 1, 1),
			LocalDate.of(2030, 1, 7));

		//Total
		assertEquals(7, result.size());

		//One per day
		expectedDates.forEach(date -> assertEquals(1,
			result.stream()
				.filter(nonRecurringSession -> nonRecurringSession.getDate().isEqual(date))
				.count()));

		// All based on RECURRING_SESSION_WITH_ID
		assert (result.stream()
			.allMatch(nonRecurringSession ->
				nonRecurringSession.getId().startsWith(RECURRING_SESSION_WITH_ID.getId())
					&& nonRecurringSession.getId().matches("^[a-z0-9]+:[0-9]{4}-[0-9]{2}-[0-9]{2}")
					&& nonRecurringSession.getCoach().equals(RECURRING_SESSION_WITH_ID.getCoach())
					&& nonRecurringSession.getDuration().equals(RECURRING_SESSION_WITH_ID.getDuration())
					&& nonRecurringSession.getStartTime().equals(RECURRING_SESSION_WITH_ID.getSchedule().getStartTime())));
	}

	@Test
	public void canGetTimetableWithWeeklySession() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_WEEKLY));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2030, 1, 1),
			LocalDate.of(2030, 1, 7));

		assertEquals(1, result.size());
		assertEquals(LocalDate.of(2030, 1, 1), result.get(0).getDate());
		assertEquals(LocalTime.of(12, 30), result.get(0).getStartTime());
		assertEquals("Title", result.get(0).getTitle());
		assertEquals("123", result.get(0).getCoach());
	}

	@Test
	public void canGetTimetableWithMonthlySession() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_MONTHLY));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2030, 1, 1),
			LocalDate.of(2030, 1, 7));

		assertEquals(1, result.size());
		assertEquals(LocalDate.of(2030, 1, 1), result.get(0).getDate());
		assertEquals(LocalTime.of(12, 30), result.get(0).getStartTime());
		assertEquals("Title", result.get(0).getTitle());
		assertEquals("123", result.get(0).getCoach());
	}

	@Test
	public void canGetTimetableWithCorrectNumberSessions() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(
				NON_RECURRING_SESSION_WITH_ID,
				RECURRING_SESSION_WITH_ID,
				RECURRING_SESSION_WEEKLY,
				RECURRING_SESSION_MONTHLY));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2030, 1, 1),
			LocalDate.of(2030, 1, 7));

		assertEquals(10, result.size());
	}

	@Test
	public void canGetTimetableWithModsApplied() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(
				RECURRING_SESSION_WITH_MODS));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2030, 1, 1),
			LocalDate.of(2030, 1, 7));

		assertEquals(6, result.size());

		assertEquals(0, result.stream()
			.filter(session -> session.getDate().equals(CANCELLATION_DATE))
			.count());
		assertEquals(1, result.stream()
			.filter(session -> session.getDate().equals(CHANGE_DATE)
			&& session.getCoach().equals("newValue")
			&& session.getStartTime().equals(LocalTime.of(14, 0)))
			.count());
	}

	@Test
	public void timetableEmptyBeforeDailySessionsStarts() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_WITH_ID));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2022, 1, 1),
			LocalDate.of(2022, 1, 7));

		assert(Collections.emptyList().equals(result));
	}
	@Test
	public void timetableEmptyAfterDailySessionsEnds() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_WITH_ID));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2022, 2, 1),
			LocalDate.of(2022, 2, 7));

		assert(Collections.emptyList().equals(result));
	}

	@Test
	public void timetableEmptyBeforeWeeklySessionsStart() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_WEEKLY));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2022, 1, 1),
			LocalDate.of(2022, 1, 7));

		assert(Collections.emptyList().equals(result));
	}

	@Test
	public void timetableEmptyAfterWeeklySessionsEnd() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_WEEKLY));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2030, 2, 1),
			LocalDate.of(2030, 2, 7));

		assert(Collections.emptyList().equals(result));
	}

	@Test
	public void timetableEmptyBeforeMonthlySessionsStart() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_MONTHLY));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2022, 1, 1),
			LocalDate.of(2022, 1, 7));

		assert(Collections.emptyList().equals(result));
	}

	@Test
	public void timetableEmptyAfterMonthlySessionsEnd() {
		when(sessionService.getAllSessions())
			.thenReturn(List.of(RECURRING_SESSION_MONTHLY));

		List<NonRecurringSession> result = timetableService.getTimetable(
			LocalDate.of(2030, 2, 1),
			LocalDate.of(2030, 2, 7));

		assert(Collections.emptyList().equals(result));
	}
}
