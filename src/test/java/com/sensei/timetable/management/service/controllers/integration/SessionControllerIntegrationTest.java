package com.sensei.timetable.management.service.controllers.integration;

import com.sensei.timetable.management.service.models.exceptions.SessionNotFoundException;
import com.sensei.timetable.management.service.services.SessionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static com.sensei.timetable.management.service.TestConstants.NON_RECURRING_SESSION_WITH_ID;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_WITH_ID;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_WITH_MODS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class SessionControllerIntegrationTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	SessionService service;

	@Test
	public void createNewNonRecurringSession() throws Exception {
		when(service.saveSession(any())).thenReturn(NON_RECURRING_SESSION_WITH_ID);

		mockMvc.perform(post("/api/v1/sessions")
			.contentType(MediaType.APPLICATION_JSON)
			.content("""
				{
				    "type": "non-recurring",
				    "title": "Non-recurring session",
				    "coach": "coachId",
				    "duration": {
				        "value": 1,
				        "unit": "HOURS"
				    },
				    "date": "2030-01-01",
				    "startTime": "12:30"
				}
				"""))
			.andDo(print())
			.andExpect(status().isCreated())
			.andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
			.andExpect(content().json("""
				{
				    "type": "non-recurring",
				    "id": "2",
				    "title": "Non-recurring session",
				    "coach": "coachId",
				    "duration": {
				        "value": 1,
				        "unit": "HOURS"
				    },
				    "date": "2030-01-01",
				    "startTime": "12:30"
				}
				""", true));
	}

	@Test
	public void createNewRecurringSession() throws Exception {
		when(service.saveSession(any())).thenReturn(RECURRING_SESSION_WITH_ID);

		mockMvc.perform(post("/api/v1/sessions")
			.contentType(MediaType.APPLICATION_JSON)
			.content("""
				{
				    "type": "recurring",
				    "title": "Recurring session",
				    "coach": "coachId",
				    "duration": {
				        "value": 1,
				        "unit": "HOURS"
				    },
				    "schedule": {
				        "startTime": "12:30",
				        "frequency": "DAILY",
				        "from": "2030-01-01",
				        "until": "2030-01-28"
				    }
				}
				"""))
			.andDo(print())
			.andExpect(status().isCreated())
			.andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
			.andExpect(content().json("""
				{
				    "type": "recurring",
				    "id": "1",
				    "title": "Recurring session",
				    "coach": "coachId",
				    "duration": {
				        "value": 1,
				        "unit": "HOURS"
				    },
				    "schedule": {
				        "startTime": "12:30",
				        "frequency": "DAILY",
				        "from": "2030-01-01",
				        "until": "2030-01-28"
				    }
				}
				""", true));
	}

	@Test
	public void createNewSessionReturns400WhenIDPresent() throws Exception {
		mockMvc.perform(post("/api/v1/sessions")
			.contentType(MediaType.APPLICATION_JSON)
			.content("""
				{
				    "type": "recurring",
				    "id": "1",
				    "title": "Recurring session",
				    "coach": "coachId",
				    "duration": {
				        "value": 1,
				        "unit": "HOURS"
				    },
				    "schedule": {
				        "startTime": "12:30",
				        "frequency": "DAILY",
				        "from": "2030-01-01",
				        "until": "2030-01-02"
				    }
				}
				"""))
			.andDo(print())
			.andExpect(status().isBadRequest());

		verifyNoInteractions(service);
	}

	@Test
	public void findAll() throws Exception {
		when(service.getAllSessions()).thenReturn(List.of(
			RECURRING_SESSION_WITH_ID,
			NON_RECURRING_SESSION_WITH_ID,
			RECURRING_SESSION_WITH_MODS
			));

		mockMvc.perform(get("/api/v1/sessions"))
			.andDo(print())
			.andExpect(status().is2xxSuccessful())
			.andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
			.andExpect(content().json("""
				[
				    {
				        "type": "recurring",
				        "id": "1",
				        "title": "Recurring session",
				        "coach": "coachId",
				        "duration": {
				            "value": 1,
				            "unit": "HOURS"
				        },
				        "schedule": {
				            "startTime": "12:30",
				            "frequency": "DAILY",
					        "from": "2030-01-01",
					        "until": "2030-01-28"
				        }
				    },
				    {
				        "type": "non-recurring",
				        "id": "2",
				        "title": "Non-recurring session",
				        "coach": "coachId",
				        "duration": {
				            "value": 1,
				            "unit": "HOURS"
				        },
				        "date": "2030-01-01",
				        "startTime": "12:30"
				    },
				    {
				        "type": "recurring",
				        "id": "3",
				        "title": "Recurring session",
				        "coach": "coachId",
				        "duration": {
				            "value": 1,
				            "unit": "HOURS"
				        },
				        "schedule": {
				            "frequency": "DAILY",
				            "from": "2030-01-01",
				            "until": "2030-01-28",
				            "startTime": "12:30",
				            "modifications": [
				                {
				                    "type": "cancellation",
				                    "affectedDates": [
				                        "2030-01-02"
				                    ]
				                },
				                {
				                    "type": "coachChange",
				                    "affectedDates": [
				                        "2030-01-03"
				                    ],
				                    "newCoach": "newValue"
				                },
				                {
				                    "type": "timeChange",
				                    "affectedDates": [
				                        "2030-01-03"
				                    ],
				                    "newStartTime": "14:00:00"
				                }
				            ]
				        }
				    }
				]
				""", true));
	}

	@Test
	public void findById() throws Exception {
		when(service.getSession("1")).thenReturn(RECURRING_SESSION_WITH_ID);

		mockMvc.perform(get("/api/v1/sessions/1"))
			.andDo(print())
			.andExpect(status().is(200))
			.andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
			.andExpect(content().json("""
				{
					"type": "recurring",
					"id": "1",
					"title": "Recurring session",
					"coach": "coachId",
					"duration": {
						"value": 1,
						"unit": "HOURS"
					},
					"schedule": {
						"startTime": "12:30",
						"frequency": "DAILY",
				        "from": "2030-01-01",
				        "until": "2030-01-28"
					}
				}
				""", true));
	}

	@Test
	public void findByIdReturns404() throws Exception {
		when(service.getSession("1")).thenThrow(new SessionNotFoundException("Session not found"));

		mockMvc.perform(get("/api/v1/sessions/1"))
			.andDo(print())
			.andExpect(status().isNotFound());
	}

	@Test
	public void update() throws Exception {
		when(service.updateSession(eq("1"), any())).thenReturn(RECURRING_SESSION_WITH_ID);

		mockMvc.perform(put("/api/v1/sessions/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
				{
				    "type": "recurring",
				    "id": "1",
				    "title": "Recurring session",
				    "coach": "coachId",
				    "duration": {
				        "value": 1,
				        "unit": "HOURS"
				    },
				    "schedule": {
				        "startTime": "12:30",
				        "frequency": "DAILY",
				        "from": "2030-01-01",
				        "until": "2030-01-02"
				    }
				}
				"""))
			.andDo(print())
			.andExpect(status().is(200))
			.andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
			.andExpect(content().json("""
				{
					"type": "recurring",
					"id": "1",
					"title": "Recurring session",
					"coach": "coachId",
					"duration": {
						"value": 1,
						"unit": "HOURS"
					},
					"schedule": {
						"startTime": "12:30",
						"frequency": "DAILY",
				        "from": "2030-01-01",
				        "until": "2030-01-28"
					}
				}
				""", true));	}

	@Test
	public void updateWithMods() throws Exception {
		when(service.updateSession(eq("3"), any())).thenReturn(RECURRING_SESSION_WITH_MODS);

		mockMvc.perform(put("/api/v1/sessions/3")
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
				{
				        "type": "recurring",
				        "id": "3",
				        "title": "Recurring session",
				        "coach": "coachId",
				        "duration": {
				            "value": 1,
				            "unit": "HOURS"
				        },
				        "schedule": {
				            "frequency": "DAILY",
				            "from": "2030-01-01",
				            "until": "2030-01-28",
				            "startTime": "12:30",
				            "modifications": [
				                {
				                    "type": "cancellation",
				                    "affectedDates": [
				                        "2030-01-02"
				                    ]
				                },
				                {
				                    "type": "coachChange",
				                    "affectedDates": [
				                        "2030-01-03"
				                    ],
				                    "newCoach": "newValue"
				                },
				                {
				                    "type": "timeChange",
				                    "affectedDates": [
				                        "2030-01-03"
				                    ],
				                    "newStartTime": "14:00:00"
				                }
				            ]
				        }
				    }
				"""))
			.andDo(print())
			.andExpect(status().is(200))
			.andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
			.andExpect(content().json("""
				{
				        "type": "recurring",
				        "id": "3",
				        "title": "Recurring session",
				        "coach": "coachId",
				        "duration": {
				            "value": 1,
				            "unit": "HOURS"
				        },
				        "schedule": {
				            "frequency": "DAILY",
				            "from": "2030-01-01",
				            "until": "2030-01-28",
				            "startTime": "12:30",
				            "modifications": [
				                {
				                    "type": "cancellation",
				                    "affectedDates": [
				                        "2030-01-02"
				                    ]
				                },
				                {
				                    "type": "coachChange",
				                    "affectedDates": [
				                        "2030-01-03"
				                    ],
				                    "newCoach": "newValue"
				                },
				                {
				                    "type": "timeChange",
				                    "affectedDates": [
				                        "2030-01-03"
				                    ],
				                    "newStartTime": "14:00:00"
				                }
				            ]
				        }
				    }
				""", true));	}

	@Test
	public void updateReturns400IfIDsDoNotMatch() throws Exception {
		mockMvc.perform(put("/api/v1/sessions/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
				{
				    "type": "recurring",
				    "id": "2",
				    "title": "Recurring session",
				    "coach": "coachId",
				    "duration": {
				        "value": 1,
				        "unit": "HOURS"
				    },
				    "schedule": {
				        "startTime": "12:30",
				        "frequency": "DAILY",
				        "from": "2030-01-01",
				        "until": "2030-01-02"
				    }
				}
				"""))
			.andDo(print())
			.andExpect(status().isBadRequest())
			.andExpect(header().string(CONTENT_TYPE, APPLICATION_JSON_VALUE))
			.andExpect(content().json("{\"error\":\"ID must match in path and body\"}", true));

		verifyNoInteractions(service);
	}

	@Test
	public void updateReturns404IfExistingSessionNotFound() throws Exception {
		when(service.updateSession(eq("1"), any())).thenThrow(new SessionNotFoundException("Session not found"));

		mockMvc.perform(put("/api/v1/sessions/1")
				.contentType(MediaType.APPLICATION_JSON)
				.content("""
				{
				    "type": "recurring",
				    "id": "1",
				    "title": "Recurring session",
				    "coach": "coachId",
				    "duration": {
				        "value": 1,
				        "unit": "HOURS"
				    },
				    "schedule": {
				        "startTime": "12:30",
				        "frequency": "DAILY",
				        "from": "2030-01-01",
				        "until": "2030-01-02"
				    }
				}
				"""))
			.andDo(print())
			.andExpect(status().isNotFound());
	}

	@Test
	public void deleteReturns200() throws Exception {
		mockMvc.perform(delete("/api/v1/sessions/1"))
			.andDo(print())
			.andExpect(status().isNoContent());

		verify(service).deleteSession("1");
	}


}
