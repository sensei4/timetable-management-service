package com.sensei.timetable.management.service.repositories;


import com.sensei.timetable.management.service.models.Duration;
import com.sensei.timetable.management.service.models.NonRecurringSession;
import com.sensei.timetable.management.service.models.RecurringSession;
import com.sensei.timetable.management.service.models.Session;
import com.sensei.timetable.management.service.models.enums.DurationUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MongoDBContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static com.sensei.timetable.management.service.TestConstants.DAILY_SCHEDULE;
import static com.sensei.timetable.management.service.TestConstants.NON_RECURRING_SESSION_WITH_ID;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_WITH_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@Testcontainers
@DataMongoTest
public class SessionRepositoryTest {

	@Container
	static MongoDBContainer mongoDBContainer = new MongoDBContainer("mongo:6.0.5");

	@DynamicPropertySource
	static void setProperties(DynamicPropertyRegistry registry) {
		registry.add("spring.data.mongodb.uri", mongoDBContainer::getReplicaSetUrl);
	}

	@Autowired
	private SessionRepository repository;

	@BeforeEach
	public void setup() {
		repository.deleteAll();
	}

	@Test
	public void saveNewSession_Recurring() {
		RecurringSession newSession = RecurringSession.builder()
			.title("Recurring session")
			.coach("coachId")
			.duration(new Duration(1, DurationUnit.HOURS))
			.schedule(DAILY_SCHEDULE)
			.build();
		assertEquals(0, repository.count());

		final Session result = repository.save(newSession);

		assertNotNull(result);
		assertNotNull(result.getId()); // Ensuring new record is assigned an ID
		assertEquals(1, repository.count());

		// Setting ID to null as it has already been verified
		// setting as null allows it to be compared to the original item
		result.setId(null);
		assertEquals(newSession, result);
	}

	@Test
	public void saveNewSession_NonRecurring() {
		NonRecurringSession newSession = NonRecurringSession.builder()
			.title("Non-recurring session")
			.coach("coachId")
			.duration(new Duration(1, DurationUnit.HOURS))
			.date(LocalDate.of(2020, 1, 1))
			.startTime(LocalTime.of(12, 30))
			.build();
		assertEquals(0, repository.count());
		final Session result = repository.save(newSession);

		assertNotNull(result);
		assertNotNull(result.getId()); // Ensuring new record is assigned an ID
		assertEquals(1, repository.count());

		// Setting ID to null as it has already been verified
		// setting as null allows it to be compared to the saved item
		result.setId(null);
		assertEquals(newSession, result);
	}

	@Test
	public void getAllSessions() {
		final List<Session> sessions = List.of(RECURRING_SESSION_WITH_ID, NON_RECURRING_SESSION_WITH_ID);
		repository.saveAll(sessions);

		final List<Session> result = repository.findAll();
		assertEquals(2, result.size());
		assertEquals(sessions, result);
	}

	@Test
	public void getSessionById() {
		repository.save(RECURRING_SESSION_WITH_ID);

		final Optional<Session> result = repository.findById(RECURRING_SESSION_WITH_ID.getId());

		assertEquals(RECURRING_SESSION_WITH_ID, result.get());
	}

	@Test
	public void updateSession() {
		repository.save(RECURRING_SESSION_WITH_ID);
		final Session updatedSession = NonRecurringSession.builder()
			.id(RECURRING_SESSION_WITH_ID.getId())
			.coach("newCoach")
			.title("newTitle")
			.build();

		repository.save(updatedSession);
		final Optional<Session> result = repository.findById(RECURRING_SESSION_WITH_ID.getId());

		assertEquals(updatedSession, result.get());
	}

	@Test
	public void delete() {
		repository.save(RECURRING_SESSION_WITH_ID);
		assertEquals(1, repository.count());

		repository.delete(RECURRING_SESSION_WITH_ID);

		assertEquals(0, repository.count());
	}

	@Test
	public void deleteById() {
		repository.save(RECURRING_SESSION_WITH_ID);
		assertEquals(1, repository.count());

		repository.deleteById(RECURRING_SESSION_WITH_ID.getId());

		assertEquals(0, repository.count());
	}

	@Test
	public void deleteAll() {
		repository.saveAll(List.of(RECURRING_SESSION_WITH_ID,
			NON_RECURRING_SESSION_WITH_ID));
		assertEquals(2, repository.count());

		repository.deleteAll();

		assertEquals(0, repository.count());
	}

	@Test
	public void count() {
		assertEquals(0, repository.count());

		repository.save(RECURRING_SESSION_WITH_ID);
		assertEquals(1, repository.count());
	}
}
