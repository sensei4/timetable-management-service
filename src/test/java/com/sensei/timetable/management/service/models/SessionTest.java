package com.sensei.timetable.management.service.models;

import com.sensei.timetable.management.service.models.enums.DurationUnit;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.sensei.timetable.management.service.TestConstants.DAILY_SCHEDULE;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SessionTest {

	@Test
	public void generatesCorrectDiff_recurring() {
		RecurringSession original = RecurringSession.builder()
			.id("1")
			.title("Recurring session")
			.coach("1")
			.duration(new Duration(1, DurationUnit.HOURS))
			.schedule(DAILY_SCHEDULE)
			.build();

		RecurringSession updated = RecurringSession.builder()
			.id("1")
			.title("Updated recurring session")
			.coach("2")
			.duration(new Duration(1, DurationUnit.HOURS))
			.schedule(DAILY_SCHEDULE)
			.build();

		List<String> expected = List.of("Updating title from 'Recurring session' to 'Updated recurring session'",
			"Updating coach from '1' to '2'");

		List<String> result = original.getDiffMessage(updated);

		assertEquals(expected, result);
	}
}
