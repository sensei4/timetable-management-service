package com.sensei.timetable.management.service.controllers;

import com.sensei.timetable.management.service.models.NonRecurringSession;
import com.sensei.timetable.management.service.services.TimetableService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.List;

import static com.sensei.timetable.management.service.TestConstants.NON_RECURRING_SESSION_WITH_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TimetableControllerTest {

	@Mock
	private TimetableService timetableService;

	@InjectMocks
	private TimetableController controller;

	@Test
	public void canGetTimetable() {
		LocalDate from = LocalDate.of(2030, 1, 1);
		LocalDate to = LocalDate.of(2030, 1, 7);
		List<NonRecurringSession> sessions = List.of(NON_RECURRING_SESSION_WITH_ID);
		when(timetableService.getTimetable(eq(from), eq(to)))
			.thenReturn(sessions);

		List<NonRecurringSession> result = controller.getTimetable(from, to);

		assertEquals(sessions, result);
	}
}
