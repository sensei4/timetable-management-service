package com.sensei.timetable.management.service.controllers.integration;

import com.sensei.timetable.management.service.services.TimetableService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.List;

import static com.sensei.timetable.management.service.TestConstants.NON_RECURRING_SESSION_WITH_ID;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc(addFilters = false)
public class TimetableControllerIntegrationTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	TimetableService timetableService;

	@Test
	public void canGetTimetable() throws Exception {
		LocalDate from = LocalDate.of(2030, 1, 1);
		LocalDate to = LocalDate.of(2030, 1, 7);
		when(timetableService.getTimetable(eq(from), eq(to)))
			.thenReturn(List.of(NON_RECURRING_SESSION_WITH_ID));

		mockMvc.perform(get("/api/v1/timetable?from=2030-01-01&to=2030-01-07")
			.accept(MediaType.APPLICATION_JSON))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON))
			.andExpect(content().json("""
				[
				    {
				        "type": "non-recurring",
				        "id": "2",
				        "title": "Non-recurring session",
				        "coach": "coachId",
				        "duration": {
				            "value": 1,
				            "unit": "HOURS"
				        },
				        "date": "2030-01-01",
				        "startTime": "12:30"
				    }
				]
				""", true));
	}
}
