package com.sensei.timetable.management.service;

import com.sensei.timetable.management.service.models.Duration;
import com.sensei.timetable.management.service.models.NonRecurringSession;
import com.sensei.timetable.management.service.models.RecurringSession;
import com.sensei.timetable.management.service.models.Schedule;
import com.sensei.timetable.management.service.models.enums.DurationUnit;
import com.sensei.timetable.management.service.models.modifiers.Cancelled;
import com.sensei.timetable.management.service.models.modifiers.CoachChange;
import com.sensei.timetable.management.service.models.modifiers.StartTimeChange;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.List;

import static com.sensei.timetable.management.service.models.enums.ScheduleFrequency.DAILY;
import static com.sensei.timetable.management.service.models.enums.ScheduleFrequency.MONTHLY;
import static com.sensei.timetable.management.service.models.enums.ScheduleFrequency.WEEKLY;

public class TestConstants {

	public static final Schedule DAILY_SCHEDULE = new Schedule(
		DAILY,
		LocalDate.of(2030, 1, 1),
		LocalDate.of(2030, 1, 28),
		LocalTime.of(12, 30),
		null);
	private static final Schedule WEEKLY_SCHEDULE = new Schedule(
		WEEKLY,
		LocalDate.of(2030, 1, 1),
		LocalDate.of(2030, 1, 28),
		LocalTime.of(12, 30),
		Collections.emptyList());
	private static final Schedule MONTHLY_SCHEDULE = new Schedule(
		MONTHLY,
		LocalDate.of(2030, 1, 1),
		LocalDate.of(2030, 1, 28),
		LocalTime.of(12, 30),
		Collections.emptyList());

	public static final LocalDate CANCELLATION_DATE = LocalDate.of(2030, 1, 2);
	private static final Cancelled CANCELLED = Cancelled.builder()
		.affectedDates(List.of(CANCELLATION_DATE))
		.build();

	public static final LocalDate CHANGE_DATE = LocalDate.of(2030, 1, 3);
	private static final CoachChange COACH_CHANGE = CoachChange.builder()
		.affectedDates(List.of(CHANGE_DATE))
		.newCoach("newValue")
		.build();

	private static final StartTimeChange START_TIME_CHANGE = StartTimeChange.builder()
		.affectedDates(List.of(CHANGE_DATE))
		.newStartTime(LocalTime.of(14, 0))
		.build();

	public static final Schedule DAILY_SCHEDULE_WITH_MODS = new Schedule(
		DAILY,
		LocalDate.of(2030, 1, 1),
		LocalDate.of(2030, 1, 28),
		LocalTime.of(12, 30),
		List.of(CANCELLED,
			COACH_CHANGE,
			START_TIME_CHANGE
		));

	public static final NonRecurringSession NON_RECURRING_SESSION_WITH_ID = NonRecurringSession.builder()
		.id("2")
		.title("Non-recurring session")
		.coach("coachId")
		.duration(new Duration(1, DurationUnit.HOURS))
		.date(LocalDate.of(2030, 1, 1))
		.startTime(LocalTime.of(12, 30))
		.build();

	public static final RecurringSession RECURRING_SESSION_WITH_ID = RecurringSession.builder()
		.id("1")
		.title("Recurring session")
		.coach("coachId")
		.duration(new Duration(1, DurationUnit.HOURS))
		.schedule(DAILY_SCHEDULE)
		.build();

	public static final RecurringSession RECURRING_SESSION_WITH_MODS = RecurringSession.builder()
		.id("3")
		.title("Recurring session")
		.coach("coachId")
		.duration(new Duration(1, DurationUnit.HOURS))
		.schedule(DAILY_SCHEDULE_WITH_MODS)
		.build();

	public static final RecurringSession RECURRING_SESSION_WEEKLY = RecurringSession.builder()
		.id("1")
		.title("Title")
		.coach("123")
		.duration(new Duration(2, DurationUnit.HOURS))
		.schedule(WEEKLY_SCHEDULE)
		.build();

	public static final RecurringSession RECURRING_SESSION_MONTHLY = RecurringSession.builder()
		.id("1")
		.title("Title")
		.coach("123")
		.duration(new Duration(2, DurationUnit.HOURS))
		.schedule(MONTHLY_SCHEDULE)
		.build();
}
