package com.sensei.timetable.management.service.services;


import com.sensei.timetable.management.service.models.Duration;
import com.sensei.timetable.management.service.models.NonRecurringSession;
import com.sensei.timetable.management.service.models.RecurringSession;
import com.sensei.timetable.management.service.models.Session;
import com.sensei.timetable.management.service.models.enums.DurationUnit;
import com.sensei.timetable.management.service.models.exceptions.IllegalSessionUpdateException;
import com.sensei.timetable.management.service.models.exceptions.SessionNotFoundException;
import com.sensei.timetable.management.service.repositories.SessionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static com.sensei.timetable.management.service.TestConstants.DAILY_SCHEDULE;
import static com.sensei.timetable.management.service.TestConstants.NON_RECURRING_SESSION_WITH_ID;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_WITH_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SessionServiceTest {

	@Mock
	SessionRepository repository;

	@InjectMocks
	SessionService service;

	@Test
	public void canSaveNewRecurringSession() {
		RecurringSession newSession = RecurringSession.builder()
			.title("Recurring session")
			.coach("coachId")
			.duration(new Duration(1, DurationUnit.HOURS))
			.schedule(DAILY_SCHEDULE)
			.build();

		service.saveSession(newSession);

		verify(repository).save(newSession);
	}

	@Test
	public void canSaveNewNonRecurringSession() {
		NonRecurringSession newSession = NonRecurringSession.builder()
			.title("Non-recurring session")
			.coach("coachId")
			.duration(new Duration(1, DurationUnit.HOURS))
			.date(LocalDate.of(2020, 1, 1))
			.startTime(LocalTime.of(12, 30))
			.build();

		service.saveSession(newSession);

		verify(repository).save(newSession);
	}

	@Test
	public void canGetAllSessions() {
		final List<Session> sessions = List.of(RECURRING_SESSION_WITH_ID, NON_RECURRING_SESSION_WITH_ID);
		when(repository.findAll()).thenReturn(sessions);

		final List<Session> result = service.getAllSessions();

		assertEquals(sessions, result);
	}

	@Test
	public void canSessionByID() throws SessionNotFoundException {
		when(repository.findById("1")).thenReturn(Optional.ofNullable(RECURRING_SESSION_WITH_ID));

		final Session result = service.getSession("1");

		assertEquals(RECURRING_SESSION_WITH_ID, result);
	}

	@Test
	public void throwsExceptionWhenSessionNotFound() {
		when(repository.findById("1")).thenReturn(Optional.empty());

		assertThrows(SessionNotFoundException.class,
			()  -> service.getSession("1"));
	}

	@Test
	public void canUpdateSession() throws SessionNotFoundException {
		final Session updated = RecurringSession.builder()
			.id("1")
			.title("Recurring session")
			.coach("coachId")
			.duration(new Duration(1, DurationUnit.HOURS))
			.schedule(DAILY_SCHEDULE)
			.build();
		when(repository.findById("1")).thenReturn(Optional.ofNullable(RECURRING_SESSION_WITH_ID));

		service.updateSession("1", updated);

		verify(repository).save(updated);
	}

	@Test
	public void cannotUpdateSessionToOtherType() throws SessionNotFoundException {
		final Session updated = NonRecurringSession.builder()
			.id("1")
			.title("Recurring session")
			.coach("coachId")
			.duration(new Duration(1, DurationUnit.HOURS))
			.date(LocalDate.of(2020, 1, 1))
			.startTime(LocalTime.of(12, 30))
			.build();
		when(repository.findById("1")).thenReturn(Optional.ofNullable(RECURRING_SESSION_WITH_ID));

		assertThrows(IllegalSessionUpdateException.class, () ->
			service.updateSession("1", updated));
	}

	@Test
	public void canDelete() {
		service.deleteSession("1");

		verify(repository).deleteById("1");
	}



}
