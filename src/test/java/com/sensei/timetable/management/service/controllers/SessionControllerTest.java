package com.sensei.timetable.management.service.controllers;

import com.sensei.timetable.management.service.models.Duration;
import com.sensei.timetable.management.service.models.NonRecurringSession;
import com.sensei.timetable.management.service.models.RecurringSession;
import com.sensei.timetable.management.service.models.Session;
import com.sensei.timetable.management.service.models.enums.DurationUnit;
import com.sensei.timetable.management.service.models.exceptions.SessionNotFoundException;
import com.sensei.timetable.management.service.services.SessionService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import static com.sensei.timetable.management.service.TestConstants.DAILY_SCHEDULE;
import static com.sensei.timetable.management.service.TestConstants.NON_RECURRING_SESSION_WITH_ID;
import static com.sensei.timetable.management.service.TestConstants.RECURRING_SESSION_WITH_ID;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SessionControllerTest {

	@Mock
	private SessionService sessionService;

	@InjectMocks
	private SessionController controller;

	@Test
	public void saveNewSession_recurring() {
		RecurringSession newSession = RecurringSession.builder()
			.title("Recurring session")
			.coach("coachId")
			.duration(new Duration(1, DurationUnit.HOURS))
			.schedule(DAILY_SCHEDULE)
			.build();

		when(sessionService.saveSession(newSession)).thenReturn(RECURRING_SESSION_WITH_ID);
		final Session result = controller.saveNewSession(newSession);

		verify(sessionService).saveSession(eq(newSession));
		assertEquals(RECURRING_SESSION_WITH_ID, result);
	}

	@Test
	public void saveNewSession_nonRecurring() {
		NonRecurringSession newSession = NonRecurringSession.builder()
			.title("Non-recurring session")
			.coach("coachId")
			.duration(new Duration(1, DurationUnit.HOURS))
			.date(LocalDate.of(2020, 1, 1))
			.startTime(LocalTime.of(12, 30))
			.build();
		when(sessionService.saveSession(newSession)).thenReturn(NON_RECURRING_SESSION_WITH_ID);
		final Session result = controller.saveNewSession(newSession);

		verify(sessionService).saveSession(eq(newSession));
		assertEquals(NON_RECURRING_SESSION_WITH_ID, result);
	}

	@Test
	public void getAllSessions() {
		final List<Session> sessions = List.of(RECURRING_SESSION_WITH_ID, NON_RECURRING_SESSION_WITH_ID);
		when(sessionService.getAllSessions()).thenReturn(sessions);

		final List<Session> result = controller.getAllSessions();

		verify(sessionService).getAllSessions();
		assertEquals(sessions, result);
	}

	@Test
	public void getSessionById() throws SessionNotFoundException {
		when(sessionService.getSession("1")).thenReturn(RECURRING_SESSION_WITH_ID);

		final Session result = controller.getSession("1");

		verify(sessionService).getSession("1");
		assertEquals(RECURRING_SESSION_WITH_ID, result);
	}

	@Test
	public void deleteSession() {
		controller.deleteSession("1");

		verify(sessionService).deleteSession("1");
	}
}
